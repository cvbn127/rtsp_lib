//
// Created by cvbn127 on 17.12.20.
//


#include "CameraDeviceSource.h"

#include <iomanip>
#include <sstream>
#include "log.h"


int CameraDeviceSource::bufferedSize = 200000;

int CameraDeviceSource::Stats::notify(int tv_sec, int framesize) {
    m_fps++;
    m_size += framesize;
    if (tv_sec != m_fps_sec) {
        //LOGI("m_msg:%ld, fps: %d, bandwidth: %d kbps",tv_sec, m_size/128);
        m_fps_sec = tv_sec;
        m_fps = 0;
        m_size = 0;
    }
    return m_fps;
}


CameraDeviceSource *
CameraDeviceSource::createNew(UsageEnvironment &env, unsigned int queueSize, bool useThread) {
    auto source = new CameraDeviceSource(env, queueSize, useThread);
    return source;
}

// Constructor
CameraDeviceSource::CameraDeviceSource(UsageEnvironment &env, unsigned int queueSize,
                                       bool useThread)
        : FramedSource(env),
          m_in("in"),
          m_out("out"),
          m_queueSize(queueSize) {
    m_eventTriggerId = envir().taskScheduler().createEventTrigger(
            CameraDeviceSource::deliverFrameStub);
    memset(&m_thid, 0, sizeof(m_thid));
    memset(&m_mutex, 0, sizeof(m_mutex));
    memset(&m_mutex_raw, 0, sizeof(m_mutex_raw));

    // start thread
    pthread_mutex_init(&m_mutex, nullptr);
    pthread_mutex_init(&m_mutex_raw, nullptr);
    pthread_create(&m_thid, nullptr, threadStub, this);
}

// Destructor
CameraDeviceSource::~CameraDeviceSource() {
    envir().taskScheduler().deleteEventTrigger(m_eventTriggerId);
    pthread_join(m_thid, nullptr);
    pthread_mutex_destroy(&m_mutex);
    pthread_mutex_destroy(&m_mutex_raw);
}

// thread mainloop
void *CameraDeviceSource::thread() {
    while (true) {
        this->getNextFrame();
    }
}

// getting FrameSource callback
void CameraDeviceSource::doGetNextFrame() {
    deliverFrame();
}

// stopping FrameSource callback
void CameraDeviceSource::doStopGettingFrames() {
    LOGI("CameraDeviceSource::doStopGettingFrames");
    FramedSource::doStopGettingFrames();
}

// deliver frame to the sink
void CameraDeviceSource::deliverFrame() {
    if (!isCurrentlyAwaitingData()) {
        return;
    }
    fDurationInMicroseconds = 0;
    fFrameSize = 0;

    pthread_mutex_lock(&m_mutex);
    if (m_captureQueue.empty()) {
        // LOGI("Queue is empty");
    } else {
        timeval curTime{};
        gettimeofday(&curTime, nullptr);
        Frame *frame = m_captureQueue.front();
        m_captureQueue.pop_front();

        m_out.notify(curTime.tv_sec, frame->m_size);
        if (frame->m_size > fMaxSize) {
            fFrameSize = fMaxSize;
            fNumTruncatedBytes = frame->m_size - fMaxSize;
        } else {
            fFrameSize = frame->m_size;
        }

        fPresentationTime = frame->m_timestamp;

        memcpy(fTo, frame->m_buffer, fFrameSize);
        delete frame;
    }
    pthread_mutex_unlock(&m_mutex);

    if (fFrameSize > 0) {
        // send Frame to the consumer
        FramedSource::afterGetting(this);
    }
}

// FrameSource callback on read event
void CameraDeviceSource::incomingPacketHandler() {
    if (this->getNextFrame() <= 0) {
        handleClosure(this);
    }
}


void CameraDeviceSource::pushRawData(char *d, unsigned int dataSize) {

    pthread_mutex_lock(&m_mutex_raw);

    m_rawDataQueue.push_back(new RawData(d, dataSize));

    pthread_mutex_unlock(&m_mutex_raw);
}


// read from device
int CameraDeviceSource::getNextFrame() {
    timeval ref;
    gettimeofday(&ref, nullptr);

    int frameSize = 0;

    pthread_mutex_lock(&m_mutex_raw);

    if (m_rawDataQueue.empty()) {
        // LOGI("rawDataQueue is empty");
    } else {
        //LOGI("Queue pop front RawData");
        auto rawData = m_rawDataQueue.front();
        m_rawDataQueue.pop_front();
        timeval tv;
        gettimeofday(&tv, nullptr);
        frameSize = rawData->m_size;
        m_in.notify(tv.tv_sec, frameSize);

        processFrame(rawData->m_buffer, frameSize, ref);
    }
    pthread_mutex_unlock(&m_mutex_raw);
    return frameSize;
}


void CameraDeviceSource::processFrame(char *frame, int frameSize, const timeval &ref) {

    auto frameList = splitFrames((unsigned char*)frame, frameSize);
    while (!frameList.empty())
    {
        auto& frame_pair = frameList.front();
        size_t size = frame_pair.second;
        char* buf = new char[size];
        memcpy(buf, frame_pair.first, size);
        queueFrame(buf,size,ref);

        frameList.pop_front();
    }
}

// post a frame to fifo
void CameraDeviceSource::queueFrame(char *frame, int frameSize, const timeval &tv) {
    pthread_mutex_lock(&m_mutex);
    while (m_captureQueue.size() >= m_queueSize) {
        //LOGI("Queue full size drop frame size:%d",(int)m_captureQueue.size());
        delete m_captureQueue.front();
        m_captureQueue.pop_front();
    }
    m_captureQueue.push_back(new Frame(frame, frameSize, tv));
    pthread_mutex_unlock(&m_mutex);

    // post an event to ask to deliver the frame
    envir().taskScheduler().triggerEvent(m_eventTriggerId, this);
}

// split packet in frames
std::list< std::pair<unsigned char*,size_t> > CameraDeviceSource::splitFrames(unsigned char* frame, unsigned frameSize)
{
    std::list< std::pair<unsigned char*,size_t> > frameList;
    if (frame != nullptr)
    {
        frameList.emplace_back(frame, frameSize);
    }
    else
    {
        //LOGI("CameraDeviceSource::splitFrames  frame empty");
    }
    return frameList;
}