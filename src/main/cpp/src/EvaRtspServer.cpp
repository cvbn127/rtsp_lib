//
// Created by cvbn127 on 17.12.20.
//

#include <type_traits>

#include "EvaRtspServer.h"
#include "log.h"

#include <UsageEnvironment.hh>
#include <BasicUsageEnvironment.hh>
#include <RTSPServer.hh>
#include <Media.hh>

#include "H264_CameraDeviceSource.h"
#include "JPEG_CameraDeviceSource.h"
#include "H264_StreamReplicator.h"
#include "JPEG_StreamReplicator.h"
#include "ServerMediaSubsession.h"

EvaRtspServer::EvaRtspServer() : serverMediaSession{nullptr} {
    scheduler = std::unique_ptr<TaskScheduler>(BasicTaskScheduler::createNew());
    usage_environment = BasicUsageEnvironment::createNew(*scheduler);
}

std::string
EvaRtspServer::init(CodecType codecType, const std::string &stream_name, const std::size_t port) {

    stop();

    if (!mainThreadStarted) {
        mainThread = std::thread([this]() {
            loop();
        });
    }

    rtspServer = RTSPServer::createNew(*usage_environment, port);

    if (rtspServer == nullptr) {
        *usage_environment << "Failed to create RTSP server: " << usage_environment->getResultMsg()
                           << "\n";\
        LOGI("unable to create RTSP server");
        return "";
    }

    serverMediaSession = ServerMediaSession::createNew(*usage_environment, stream_name.c_str());

    if (codecType == CodecType::H264) {
        realSource = H264_DeviceByteSource::createNew(*usage_environment, 40, true);
        dataSource = H264_CameraDeviceSource::createNew(*usage_environment,
                                                        (H264_DeviceByteSource *) realSource);
        auto *replicator = H264_StreamReplicator::createNew(*usage_environment, dataSource, false);
        using replicator_type = std::remove_pointer<decltype(replicator)>::type;
        serverMediaSession->addSubsession(
                UnicastServerMediaSubsession<replicator_type>::createNew(*usage_environment,
                                                                         replicator));
    } else if (codecType == CodecType::MotionJPEG) {
        realSource = DeviceByteSource::createNew(*usage_environment, 40);
        dataSource = JPEG_CameraDeviceSource::createNew(*usage_environment, realSource);
        auto *replicator = JPEG_StreamReplicator::createNew(*usage_environment,
                                                            (JPEGVideoSource *) dataSource, false);
        using replicator_type = std::remove_pointer<decltype(replicator)>::type;
        serverMediaSession->addSubsession(
                UnicastServerMediaSubsession<replicator_type>::createNew(*usage_environment,
                                                                         replicator));
    }

    OutPacketBuffer::maxSize = 600000;//DisplayDeviceSource::bufferedSize;

    rtspServer->addServerMediaSession(serverMediaSession);

    auto url = std::string(rtspServer->rtspURL(serverMediaSession));

    LOGI("url is ", url.c_str());

    return url;
}

bool EvaRtspServer::isStreamStarted() const {
    return streamStarted;
}

void EvaRtspServer::loop() {
    mainThreadStarted = true;
    quit = 0;
    scheduler->doEventLoop(&quit); // does not return
}

void EvaRtspServer::feedData(const char *ptr, std::size_t len) {
    realSource->feed(ptr, len);
}

void EvaRtspServer::stop() {
    quit = 1;
    mainThreadStarted = false;
    if (mainThread.joinable()) {
        mainThread.join();
    }
    if (rtspServer != nullptr) {
        rtspServer->closeAllClientSessionsForServerMediaSession(serverMediaSession);
        rtspServer->removeServerMediaSession(serverMediaSession);
    }
    if (serverMediaSession != nullptr) {
        serverMediaSession->deleteAllSubsessions();
        Medium::close(serverMediaSession);
        serverMediaSession = nullptr;
    }

    Medium::close(dataSource);
    dataSource = nullptr;

    streamStarted = false;
    if (rtspServer != nullptr) {
        Medium::close(rtspServer);
        rtspServer = nullptr;
    }
}

EvaRtspServer::~EvaRtspServer() {
    streamStarted = false;
    quit = 1;
    mainThread.join();

    Medium::close(dataSource);
    usage_environment->reclaim();
}