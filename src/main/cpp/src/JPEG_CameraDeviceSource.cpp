#include "JPEG_CameraDeviceSource.h"
#include <pthread.h>
#include "log.h"

JPEG_CameraDeviceSource *
JPEG_CameraDeviceSource::createNew(UsageEnvironment &env, FramedSource *source) {
    return new JPEG_CameraDeviceSource(env, source);
}

JPEG_CameraDeviceSource::JPEG_CameraDeviceSource(UsageEnvironment &env, FramedSource *source)
        : JPEGVideoSource(env),
          m_inputSource(source) {
    fLastHeight = 480 / 8; // if 2048 then 0
    fLastWidth = 640 / 8; // if 2048 then 0
}

JPEG_CameraDeviceSource::~JPEG_CameraDeviceSource() {
    Medium::close(m_inputSource);
}

Boolean JPEG_CameraDeviceSource::isJPEGVideoSource() const {
    return True;
}

void JPEG_CameraDeviceSource::doGetNextFrame() {
    if (m_inputSource) {
        m_inputSource->getNextFrame(fTo, fMaxSize, afterGettingFrameSub, this,
                                    FramedSource::handleClosure, this);
    }
}

void JPEG_CameraDeviceSource::doStopGettingFrames() {
    FramedSource::doStopGettingFrames();
    if (m_inputSource) {
        m_inputSource->stopGettingFrames();
    }
}

void JPEG_CameraDeviceSource::afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                                                struct timeval presentationTime,
                                                unsigned durationInMicroseconds) {
    setParamsFromHeader(fTo, frameSize);
    fNumTruncatedBytes = numTruncatedBytes;
    fPresentationTime = presentationTime;
    fDurationInMicroseconds = durationInMicroseconds;
    fFrameSize = frameSize;
    afterGetting(this);
}

void
JPEG_CameraDeviceSource::afterGettingFrameSub(void *clientData, unsigned frameSize,
                                              unsigned numTruncatedBytes,
                                              struct timeval presentationTime,
                                              unsigned durationInMicroseconds) {
    auto *source = (JPEG_CameraDeviceSource *) clientData;
    source->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime,
                              durationInMicroseconds);
}

std::string JPEG_CameraDeviceSource::getAuxLine() {
    return std::string("");
}

u_int8_t JPEG_CameraDeviceSource::type() {
    return 1;
}

u_int8_t JPEG_CameraDeviceSource::qFactor() {
    return 128;
}

u_int8_t JPEG_CameraDeviceSource::width() {
    return fLastWidth;
}

u_int8_t JPEG_CameraDeviceSource::height() {
    return fLastHeight;
}


void JPEG_CameraDeviceSource::setParamsFromHeader(unsigned char *ptr, unsigned int frameSize) {
    // Look for the "SOF0" marker (0xFF 0xC0), to get the frame
    // width and height:
    Boolean foundIt = False;
    for (int i = 0; i < frameSize - 8; ++i) {
        if (ptr[i] == MARKER_START) {
            uint8_t next_byte = ptr[i+1];
            if (next_byte == 0xC0) {
                fLastHeight = (ptr[i + 5] << 5) | (ptr[i + 6] >> 3);
                fLastWidth = (ptr[i + 7] << 5) | (ptr[i + 8] >> 3);
                foundIt = True;
            }

            else if (next_byte == 0xDB) {
                if (ptr[i+4] == 0)
                {
                    memcpy(m_qTable, ptr + i + 5, 64);
                    qTable0Init = true;
                }
                else if (ptr[i+4] == 1)
                {
                    memcpy(m_qTable + 64, ptr + i + 5, 64);
                    qTable1Init = true;
                }
            }
        }

    }
    if (!foundIt) {
        fprintf(stderr, "JPEG_CameraDeviceSource: Failed to find SOF0 marker in header!\n");
    }
}