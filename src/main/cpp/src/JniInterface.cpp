//
// Created by cvbn127 on 19.12.20.
//
#include <jni.h>
#include "log.h"

#include <UsageEnvironment.hh>

#include "EvaRtspServer.h"

static EvaRtspServer evaRtspServer;

extern "C"
{
JNIEXPORT jstring JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_initialize(JNIEnv *, jobject, jint codec_type, jstring, jint);
JNIEXPORT void JNICALL Java_com_sdbcs_eva_rtsplib_NativeRtspServer_stop(JNIEnv *, jobject);
JNIEXPORT jboolean JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_isStreamStarted(JNIEnv *, jobject);
JNIEXPORT void JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_feedData(JNIEnv *, jobject, jbyteArray);
//    JNIEXPORT void JNICALL Java_com_sdbcs_eva_NativeRtspServer_destroy(JNIEnv *, jobject);
}

extern "C" JNIEXPORT jint JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNIEnv *env;
    if (vm->GetEnv(reinterpret_cast<void **>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    // Get jclass with env->FindClass.
    // Register methods with env->RegisterNatives.
    return JNI_VERSION_1_6;
}

extern "C" JNIEXPORT jstring JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_initialize(JNIEnv *jniEnv, jobject jthiz, jint codec_type, jstring stream_name,
                                                       jint port = 8554) {
    auto native_stream_name = std::string(jniEnv->GetStringUTFChars(stream_name, 0));
    auto stream_codec_type = EvaRtspServer::CodecType::H264;
    if (codec_type == 1) {
        stream_codec_type = EvaRtspServer::CodecType::MotionJPEG;
    }
    auto url = evaRtspServer.init(stream_codec_type, native_stream_name, port);
    return jniEnv->NewStringUTF(url.c_str());
}

extern "C" JNIEXPORT jboolean JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_isStreamStarted(JNIEnv *jniEnv, jobject jthiz) {
    return evaRtspServer.isStreamStarted();
}

extern "C" JNIEXPORT void JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_stop(JNIEnv *jniEnv, jobject jthiz) {
    return evaRtspServer.stop();
}

extern "C" JNIEXPORT void JNICALL
Java_com_sdbcs_eva_rtsplib_NativeRtspServer_feedData(JNIEnv *jniEnv, jobject jthiz,
                                                         jbyteArray dataArray) {
    int len = jniEnv->GetArrayLength(dataArray);
    char *buf = new char[len];
    jniEnv->GetByteArrayRegion(dataArray, 0, len, reinterpret_cast<jbyte *>(buf));
    evaRtspServer.feedData(buf, len);
}