//
// Created by cvbn127 on 08.02.21.
//
#include "DeviceByteSource.h"

DeviceByteSource *
DeviceByteSource::createNew(UsageEnvironment &env, u_int8_t queueSize) {
    return new DeviceByteSource(env, queueSize);
}

DeviceByteSource::DeviceByteSource(UsageEnvironment &env, u_int8_t queueSize) :
        FramedSource(env), queue_size{queueSize} {
    memset(&process_data_thread_id, 0, sizeof(process_data_thread_id));
    memset(&data_mutex, 0, sizeof(data_mutex));
    memset(&frame_mutex, 0, sizeof(frame_mutex));

    pthread_mutex_init(&data_mutex, nullptr);
    pthread_mutex_init(&frame_mutex, nullptr);
    pthread_create(&process_data_thread_id, nullptr, process_data_stub, this);

    deliverFrameEventTriggerId = envir().taskScheduler().createEventTrigger(
            DeviceByteSource::deliverFrameStub);
}

DeviceByteSource::~DeviceByteSource() {
    should_stop = true;
    pthread_join(process_data_thread_id, nullptr);
    envir().taskScheduler().deleteEventTrigger(deliverFrameEventTriggerId);

    pthread_mutex_destroy(&data_mutex);
    pthread_mutex_destroy(&frame_mutex);
}

void *DeviceByteSource::process_data() {

    while (!should_stop) {

        std::pair<char *, std::size_t> raw_data = {};
        {
            pthread_mutex_lock(&data_mutex);
            if (raw_data_list.empty()) {
                raw_data.first = nullptr;
            } else {
                raw_data = raw_data_list.front();
                raw_data_list.pop_front();
            }
            pthread_mutex_unlock(&data_mutex);
        }
        if (raw_data.first == nullptr) {
            continue;
        }


        auto frameList = splitFrames(raw_data.first, raw_data.second);
        while (!frameList.empty()) {
            auto &frame_pair = frameList.front();
            size_t size = frame_pair.second;
            char *buf = new char[size];
            memcpy(buf, frame_pair.first, size);
//            queueFrame(buf,size,ref);
            {
                pthread_mutex_lock(&frame_mutex);

                while (frame_data_list.size() >= queue_size) {
                    //LOGI("Queue full size drop frame size:%d",(int)m_captureQueue.size());
                    auto obsolete = frame_data_list.front();
                    frame_data_list.pop_front();
                    delete[] obsolete.first;
                }
                frame_data_list.emplace_back(buf, size);

                pthread_mutex_unlock(&frame_mutex);
            }

            frameList.pop_front();
        }
        delete raw_data.first;
    }
    pthread_exit(0);
}

std::list<std::pair<char *, size_t> >
DeviceByteSource::splitFrames(char *frame, unsigned frameSize) {
    std::list<std::pair<char *, size_t> > frameList;
    if (frame != nullptr) {
        frameList.emplace_back(frame, frameSize);
    } else {
        //LOGI("CameraDeviceSource::splitFrames  frame empty");
    }
    return frameList;
}

void DeviceByteSource::feed(const char *ptr, std::size_t len) {
    pthread_mutex_lock(&data_mutex);

    raw_data_list.emplace_back(const_cast<char *>(ptr), len);

    pthread_mutex_unlock(&data_mutex);
}

void DeviceByteSource::doGetNextFrame() {

    if (!isCurrentlyAwaitingData()) {
        return;
    }

    std::pair<char *, std::size_t> current_frame = {};

    pthread_mutex_lock(&frame_mutex);

    if (frame_data_list.empty()) {
        current_frame.first = nullptr;
    } else {
        current_frame = frame_data_list.front();
        frame_data_list.pop_front();
    }

    pthread_mutex_unlock(&frame_mutex);

    if (current_frame.first == nullptr) {
        /*
         * Данные запрашиваются библиотекой,
         * но готовых к отправке данных ещё нет.
         *
         * Если не вызвать FramedSource::afterGetting, то в очереди колбэков больше не окажется doGetNextFrame,
         * а значит данные перестанут передаваться. Я пробовал вызывать afterGetting без данных, но получил SegFault.
         * Поэтому здесь мы тригеррим ивент вызывающий doGetNextFrame и надеемся что к моменту как doGetNextFrame будет вызвана
         * данные уже появятся в frame_data_list.
         */
        envir().taskScheduler().triggerEvent(deliverFrameEventTriggerId, this);
        return;
    }

    gettimeofday(&fPresentationTime, nullptr);

    fFrameSize = current_frame.second;
    memmove(fTo, current_frame.first, fFrameSize);
    delete[] current_frame.first;

    // Inform the downstream object that it has data:
    FramedSource::afterGetting(this);
}