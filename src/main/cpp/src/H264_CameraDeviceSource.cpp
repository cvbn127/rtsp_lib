//
// Created by cvbn127 on 17.12.20.
//

#include "H264_CameraDeviceSource.h"

#include <sstream>

// live555
#include <Base64.hh>
#include <iomanip>

#include "log.h"

H264_DeviceByteSource* H264_DeviceByteSource::createNew(UsageEnvironment &env, u_int8_t queueSize, bool repeatConfig) {
    return new H264_DeviceByteSource(env, queueSize, repeatConfig);
}

H264_DeviceByteSource::H264_DeviceByteSource(UsageEnvironment &env, u_int8_t queueSize, bool repeatConfig) : DeviceByteSource(env, queueSize), m_repeatConfig{repeatConfig}{

}
// split packet in frames
std::list<std::pair<char *, size_t> >
H264_DeviceByteSource::splitFrames(char *frame, unsigned frameSize) {
    std::list<std::pair<char *, size_t> > frameList;

    size_t bufSize = frameSize;
    size_t size = 0;
    unsigned char *buffer = this->extractFrame((unsigned char*)frame, bufSize, size);
    while (buffer != nullptr) {
        switch (buffer[0] & 0x1F) {
            case 7:
                LOGI("SPS size:%d", size);
                m_sps.assign((char *) buffer, size);
                break;
            case 8:
                LOGI("PPS size:%d", size);
                m_pps.assign((char *) buffer, size);
                break;
            case 5:
                LOGI("IDR size:%d", size);
                if (m_repeatConfig && !m_sps.empty() && !m_pps.empty()) {
                    frameList.emplace_back( (char *) m_sps.c_str(), m_sps.size());
                    frameList.emplace_back( (char *) m_pps.c_str(), m_pps.size());
                }
                break;
            default:
                break;
        }

        if (m_auxLine.empty() && !m_sps.empty() && !m_pps.empty()) {
            u_int32_t profile_level_id = 0;
            if (m_sps.size() >= 4) profile_level_id = (m_sps[1] << 16) | (m_sps[2] << 8) | m_sps[3];

            char *sps_base64 = base64Encode(m_sps.c_str(), m_sps.size());
            char *pps_base64 = base64Encode(m_pps.c_str(), m_pps.size());

            std::ostringstream os;
            os << "profile-level-id=" << std::hex << std::setw(6) << profile_level_id;
            os << ";sprop-parameter-sets=" << sps_base64 << "," << pps_base64;
            m_auxLine.assign(os.str());

            free(sps_base64);
            free(pps_base64);
            LOGI("%s", m_auxLine.c_str());
        }
        frameList.emplace_back((char *)buffer, size);

        buffer = this->extractFrame(&buffer[size], bufSize, size);
    }
    return frameList;
}

std::string H264_DeviceByteSource::getAuxLine() {
    return m_auxLine;
}

bool H264_DeviceByteSource::isStartNal(const unsigned char *ptr) const {
    return (memcmp(ptr, H264marker, H264markerSize) == 0);
}

// extract a frame
unsigned char *
H264_DeviceByteSource::extractFrame(unsigned char *frame, size_t &size, size_t &outsize) {
    unsigned char *outFrame = nullptr;
    outsize = 0;
    if ((size >= H264markerSize) && isStartNal(frame)) {
        size -= H264markerSize;
        outFrame = &frame[H264markerSize];
        outsize = size;
        for (int i = 0; i + H264markerSize < size; ++i) {
            if (isStartNal(&outFrame[i])) {
                outsize = i;
                break;
            }
        }
        size -= outsize;
    }
    return outFrame;
}

std::string H264_CameraDeviceSource::getAuxLine() {
    return m_inputSource->getAuxLine();
}