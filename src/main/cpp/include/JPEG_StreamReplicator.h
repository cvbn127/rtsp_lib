//
// Created by cvbn127 on 16.02.21.
//

#ifndef EVA_JPEG_STREAMREPLICATOR_H
#define EVA_JPEG_STREAMREPLICATOR_H

#ifndef _FRAMED_SOURCE_HH

#include <FramedSource.hh>

#endif

#include "JPEG_CameraDeviceSource.h"

class JPEG_StreamReplica;

class JPEG_StreamReplicator : public Medium {
public:
    static JPEG_StreamReplicator *createNew(UsageEnvironment &env, JPEGVideoSource *inputSource,
                                            Boolean deleteWhenLastReplicaDies = True);

    virtual FramedSource *createStreamReplica();

    unsigned numReplicas() const { return fNumReplicas; }

    FramedSource *inputSource() const { return fInputSource; }

    void detachInputSource() { fInputSource = NULL; }

    using source_type = JPEG_CameraDeviceSource;

protected:
    JPEG_StreamReplicator(UsageEnvironment &env, JPEGVideoSource *inputSource,
                          Boolean deleteWhenLastReplicaDies);

    virtual ~JPEG_StreamReplicator();

private:
    friend class JPEG_StreamReplica;

    void getNextFrame(JPEG_StreamReplica *replica);

    void deactivateStreamReplica(JPEG_StreamReplica *replica);

    void removeStreamReplica(JPEG_StreamReplica *replica);

private:
    static void afterGettingFrame(void *clientData, unsigned frameSize,
                                  unsigned numTruncatedBytes,
                                  struct timeval presentationTime,
                                  unsigned durationInMicroseconds);

    void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                           struct timeval presentationTime, unsigned durationInMicroseconds);

    static void onSourceClosure(void *clientData);

    void onSourceClosure();

    void deliverReceivedFrame();

protected:
    FramedSource *fInputSource;
    Boolean fDeleteWhenLastReplicaDies, fInputSourceHasClosed;
    unsigned fNumReplicas, fNumActiveReplicas, fNumDeliveriesMadeSoFar;
    int fFrameIndex; // 0 or 1; used to figure out if a replica is requesting the current frame, or the next frame

    JPEG_StreamReplica *fPrimaryReplica; // the first replica that requests each frame.  We use its buffer when copying to the others.
    JPEG_StreamReplica *fReplicasAwaitingCurrentFrame; // other than the 'primary' replica
    JPEG_StreamReplica *fReplicasAwaitingNextFrame; // replicas that have already received the current frame, and have asked for the next
};

////////// Definition of "StreamReplica": The class that implements each stream replica //////////

class JPEG_StreamReplica : public JPEGVideoSource {
public:
    u_int8_t type() override;

    u_int8_t qFactor() override;

    u_int8_t width() override;

    u_int8_t height() override;

    u_int8_t const *quantizationTables(u_int8_t &precision,
                                       u_int16_t &length) override;

    u_int16_t restartInterval() override;

    Boolean isJPEGVideoSource() const override;

    void doGetNextFrame() override;

    void doStopGettingFrames() override;

private:
    static void copyReceivedFrame(JPEG_StreamReplica *toReplica, JPEG_StreamReplica *fromReplica);

protected:
    friend class JPEG_StreamReplicator;

    JPEG_StreamReplica(JPEG_StreamReplicator &ourReplicator);

    ~JPEG_StreamReplica() override;

    JPEG_StreamReplicator &fOurReplicator;
    int fFrameIndex; // 0 or 1, depending upon which frame we're currently requesting; could also be -1 if we've stopped playing

    // Replicas that are currently awaiting data are kept in a (singly-linked) list:
    JPEG_StreamReplica *fNext;
};

#endif //EVA_JPEG_STREAMREPLICATOR_H
