#ifndef _EVA_JPEG_CAMERA_DEVICE_SOURCE_HH
#define _EVA_JPEG_CAMERA_DEVICE_SOURCE_HH

#ifndef _JPEG_VIDEO_SOURCE_HH

#include "JPEGVideoSource.hh"

#endif

#include <string>

class JPEG_CameraDeviceSource : public JPEGVideoSource {

public:
    static JPEG_CameraDeviceSource *createNew(UsageEnvironment &env, FramedSource *source);

    Boolean isJPEGVideoSource() const override;

    void doGetNextFrame() override;

    void doStopGettingFrames() override;

    static void
    afterGettingFrameSub(void *clientData, unsigned frameSize, unsigned numTruncatedBytes,
                         struct timeval presentationTime, unsigned durationInMicroseconds);

    void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                           struct timeval presentationTime, unsigned durationInMicroseconds);

    std::string getAuxLine();


protected:
    JPEG_CameraDeviceSource(UsageEnvironment &env, FramedSource *source);

    virtual ~JPEG_CameraDeviceSource();


private:

    virtual u_int8_t type() override;

    virtual u_int8_t qFactor() override;

    virtual u_int8_t width() override;

    virtual u_int8_t height() override;

    u_int8_t const *quantizationTables(u_int8_t &precision, u_int16_t &length) override {
        length = 0;
        precision = 0;
        if (qTable0Init && qTable1Init) {
            precision = 8;
            length = sizeof(m_qTable);
        }
        return m_qTable;
    }

private:

    void setParamsFromHeader(unsigned char *ptr, unsigned int frameSize);

private:

    u_int8_t fLastWidth, fLastHeight;
    u_int8_t m_qTable[128]{0};

    bool qTable0Init{false};
    bool qTable1Init{false};

private:
    static constexpr uint8_t MARKER_START = 0xff;

protected:
    FramedSource *m_inputSource;
};

#endif
