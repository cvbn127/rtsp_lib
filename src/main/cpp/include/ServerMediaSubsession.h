#ifndef LIVESTREAMER_SERVERMEDIASUBSESSION_H
#define LIVESTREAMER_SERVERMEDIASUBSESSION_H

#include <string>
#include <iomanip>

#include <sstream>
// live555
#include <BasicUsageEnvironment.hh>
#include <Groupsock.hh>
#include <GroupsockHelper.hh>
#include <Base64.hh>

// live555
#include <liveMedia.hh>

#include "log.h"

#include "H264_StreamReplicator.h"
#include "JPEG_StreamReplicator.h"

// forward declaration
class CameraDeviceSource;

// ---------------------------------
//   BaseServerMediaSubsession
// ---------------------------------
template<typename Replicator>
class BaseServerMediaSubsession {
public:
    BaseServerMediaSubsession(Replicator *replicator) : m_replicator(replicator) {};

public:
    static FramedSource *createSource(UsageEnvironment &env, FramedSource *videoES) {
        return H264VideoStreamDiscreteFramer::createNew(env, videoES);
    };

    static RTPSink *createSink(UsageEnvironment &env, Groupsock *rtpGroupsock,
                               unsigned char rtpPayloadTypeIfDynamic) {
        return H264VideoRTPSink::createNew(env, rtpGroupsock, rtpPayloadTypeIfDynamic);
    };

    char const *getAuxLine(FramedSource *source, unsigned char rtpPayloadType) {

        const char *auxLine = NULL;
        if (source != nullptr) {
            using real_source_type = typename Replicator::source_type;
            auto real_source = (real_source_type *) source;

            std::ostringstream os;
            os << "a=fmtp:" << int(rtpPayloadType) << " ";
            os << real_source->getAuxLine();
            os << "\r\n";
//
            auxLine = strdup(os.str().c_str());
        }

        return auxLine;
    };

protected:
    Replicator *m_replicator;
};

template<>
FramedSource *BaseServerMediaSubsession<JPEG_StreamReplicator>::createSource(UsageEnvironment &env,
                                                                             FramedSource *videoES) {
    return videoES;
}

template<>
RTPSink *BaseServerMediaSubsession<JPEG_StreamReplicator>::createSink(UsageEnvironment &env,
                                                                      Groupsock *rtpGroupsock,
                                                                      unsigned char rtpPayloadTypeIfDynamic) {
    return JPEGVideoRTPSink::createNew(env, rtpGroupsock);;
}

// -----------------------------------------
//    ServerMediaSubsession for Multicast
// -----------------------------------------
template<typename Replicator>
class MulticastServerMediaSubsession
        : public PassiveServerMediaSubsession, public BaseServerMediaSubsession<Replicator> {
public:
    static MulticastServerMediaSubsession *
    createNew(UsageEnvironment &env, struct in_addr destinationAddress, Port rtpPortNum,
              Port rtcpPortNum, int ttl, Replicator *replicator
    ) {
        // Create a source
        FramedSource *source = replicator->createStreamReplica();
        FramedSource *videoSource = BaseServerMediaSubsession<Replicator>::createSource(env,
                                                                                        source);

        // Create RTP/RTCP groupsock


        struct sockaddr_storage storage;
        struct sockaddr_in sin;
        sin.sin_addr = destinationAddress;

        memset(&sin, 0, sizeof(sin));
        sin.sin_family = AF_INET;
        memcpy(&storage, &sin, sizeof(sin));


//    Groupsock* rtpGroupsock = new Groupsock(env, destinationAddress, rtpPortNum, ttl);
//    Groupsock* rtcpGroupsock = new Groupsock(env, destinationAddress, rtcpPortNum, ttl);



        Groupsock *rtpGroupsock = new Groupsock(env, storage, rtpPortNum, ttl);
        Groupsock *rtcpGroupsock = new Groupsock(env, storage, rtcpPortNum, ttl);

        // Create a RTP sink
        RTPSink *videoSink = BaseServerMediaSubsession<Replicator>::createSink(env, rtpGroupsock,
                                                                               96);

        // Create 'RTCP instance'
        const unsigned maxCNAMElen = 100;
        unsigned char CNAME[maxCNAMElen + 1];
        gethostname((char *) CNAME, maxCNAMElen);
        CNAME[maxCNAMElen] = '\0';
        RTCPInstance *rtcpInstance = RTCPInstance::createNew(env, rtcpGroupsock, 500, CNAME,
                                                             videoSink, NULL);

        // Start Playing the Sink
        videoSink->startPlaying(*videoSource, NULL, NULL);

        return new MulticastServerMediaSubsession<Replicator>(replicator, videoSink, rtcpInstance);
    };

protected:
    MulticastServerMediaSubsession(Replicator *replicator, RTPSink *rtpSink,
                                   RTCPInstance *rtcpInstance)
            : PassiveServerMediaSubsession(*rtpSink, rtcpInstance),
              BaseServerMediaSubsession<Replicator>(replicator), m_rtpSink(rtpSink) {};

    virtual char const *sdpLines() {
        if (m_SDPLines.empty()) {
            // Ugly workaround to give SPS/PPS that are get from the RTPSink
            m_SDPLines.assign(PassiveServerMediaSubsession::sdpLines());
            m_SDPLines.append(getAuxSDPLine(m_rtpSink, NULL));
        }
        return m_SDPLines.c_str();
    };

    virtual char const *getAuxSDPLine(RTPSink *rtpSink, FramedSource *inputSource) {
        return this->getAuxLine(
                (FramedSource *) (BaseServerMediaSubsession<Replicator>::m_replicator->inputSource()),
                rtpSink->rtpPayloadType());
    };

protected:
    RTPSink *m_rtpSink;
    std::string m_SDPLines;
};

// -----------------------------------------
//    ServerMediaSubsession for Unicast
// -----------------------------------------
template<typename Replicator>
class UnicastServerMediaSubsession
        : public OnDemandServerMediaSubsession, public BaseServerMediaSubsession<Replicator> {
public:
    static UnicastServerMediaSubsession *createNew(UsageEnvironment &env, Replicator *replicator) {
        return new UnicastServerMediaSubsession<Replicator>(env, replicator);
    };

protected:
    UnicastServerMediaSubsession(UsageEnvironment &env, Replicator *replicator)
            : OnDemandServerMediaSubsession(env, False),
              BaseServerMediaSubsession<Replicator>(replicator) {};

    virtual FramedSource *createNewStreamSource(unsigned clientSessionId, unsigned &estBitrate) {
        FramedSource *source = BaseServerMediaSubsession<Replicator>::m_replicator->createStreamReplica();
        return BaseServerMediaSubsession<Replicator>::createSource(envir(), source);
    };

    virtual RTPSink *
    createNewRTPSink(Groupsock *rtpGroupsock, unsigned char rtpPayloadTypeIfDynamic,
                     FramedSource *inputSource) {
        return BaseServerMediaSubsession<Replicator>::createSink(envir(), rtpGroupsock,
                                                                 rtpPayloadTypeIfDynamic);
    };

    virtual char const *getAuxSDPLine(RTPSink *rtpSink, FramedSource *inputSource) {
        return this->getAuxLine(
                (FramedSource *) (BaseServerMediaSubsession<Replicator>::m_replicator->inputSource()),
                rtpSink->rtpPayloadType());
    };
//    virtual char const* sdpLines() override ;
};

#endif //LIVESTREAMER_SERVERMEDIASUBSESSION_H