//
// Created by cvbn127 on 16.02.21.
//

#ifndef EVA_CUSTOMSTREAMREPLICATOR_H
#define EVA_CUSTOMSTREAMREPLICATOR_H

#ifndef _FRAMED_SOURCE_HH

#include <FramedSource.hh>

#endif

#include "H264_CameraDeviceSource.h"

class H264_StreamReplica; // forward

class H264_StreamReplicator : public Medium {
public:
    static H264_StreamReplicator *createNew(UsageEnvironment &env, FramedSource *inputSource,
                                             Boolean deleteWhenLastReplicaDies = True);
    // If "deleteWhenLastReplicaDies" is True (the default), then the "H264_StreamReplicator" object is deleted when (and only when)
    //   all replicas have been deleted.  (In this case, you must *not* call "Medium::close()" on the "H264_StreamReplicator" object,
    //   unless you never created any replicas from it to begin with.)
    // If "deleteWhenLastReplicaDies" is False, then the "H264_StreamReplicator" object remains in existence, even when all replicas
    //   have been deleted.  (This allows you to create new replicas later, if you wish.)  In this case, you delete the
    //   "H264_StreamReplicator" object by calling "Medium::close()" on it - but you must do so only when "numReplicas()" returns 0.

    virtual FramedSource *createStreamReplica();

    unsigned numReplicas() const { return fNumReplicas; }

    FramedSource *inputSource() const { return fInputSource; }

    // Call before destruction if you want to prevent the destructor from closing the input source
    void detachInputSource() { fInputSource = NULL; }

    using source_type = H264_CameraDeviceSource;

protected:
    H264_StreamReplicator(UsageEnvironment &env, FramedSource *inputSource,
                           Boolean deleteWhenLastReplicaDies);

    // called only by "createNew()"
    virtual ~H264_StreamReplicator();

private:
    // Routines called by replicas to implement frame delivery, and the stopping/restarting/deletion of replicas:
    friend class H264_StreamReplica;

    void getNextFrame(H264_StreamReplica *replica);

    void deactivateStreamReplica(H264_StreamReplica *replica);

    void removeStreamReplica(H264_StreamReplica *replica);

private:
    static void afterGettingFrame(void *clientData, unsigned frameSize,
                                  unsigned numTruncatedBytes,
                                  struct timeval presentationTime,
                                  unsigned durationInMicroseconds);

    void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                           struct timeval presentationTime, unsigned durationInMicroseconds);

    static void onSourceClosure(void *clientData);

    void onSourceClosure();

    void deliverReceivedFrame();

protected:
    FramedSource *fInputSource;
    Boolean fDeleteWhenLastReplicaDies, fInputSourceHasClosed;
    unsigned fNumReplicas, fNumActiveReplicas, fNumDeliveriesMadeSoFar;
    int fFrameIndex; // 0 or 1; used to figure out if a replica is requesting the current frame, or the next frame

    H264_StreamReplica *fPrimaryReplica; // the first replica that requests each frame.  We use its buffer when copying to the others.
    H264_StreamReplica *fReplicasAwaitingCurrentFrame; // other than the 'primary' replica
    H264_StreamReplica *fReplicasAwaitingNextFrame; // replicas that have already received the current frame, and have asked for the next
};

////////// Definition of "H264_StreamReplica": The class that implements each stream replica //////////

class H264_StreamReplica : public FramedSource {
protected:
    friend class H264_StreamReplicator;

    H264_StreamReplica(
            H264_StreamReplicator &ourReplicator); // called only by "H264_StreamReplicator::createStreamReplica()"
    ~H264_StreamReplica() override;

protected: // redefined virtual functions:
    void doGetNextFrame() override;

    void doStopGettingFrames() override;

private:
    static void copyReceivedFrame(H264_StreamReplica *toReplica, H264_StreamReplica *fromReplica);

protected:
    H264_StreamReplicator &fOurReplicator;
    int fFrameIndex; // 0 or 1, depending upon which frame we're currently requesting; could also be -1 if we've stopped playing

    // Replicas that are currently awaiting data are kept in a (singly-linked) list:
    H264_StreamReplica *fNext;
};

#endif //EVA_CUSTOMSTREAMREPLICATOR_H
