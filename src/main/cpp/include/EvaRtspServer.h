//
// Created by cvbn127 on 17.12.20.
//

#ifndef EVA_EVARTSPSERVER_H
#define EVA_EVARTSPSERVER_H

#include <cstddef>
#include <memory>
#include <string>
#include <functional>
#include <thread>

class TaskScheduler;
class UsageEnvironment;
class RTSPServer;
class ServerMediaSession;

class FramedSource;
class DeviceByteSource;

class EvaRtspServer {
public:
    enum CodecType {
        MotionJPEG,
        H264
    };

    EvaRtspServer();

    std::string init(CodecType codecType, const std::string &stream_name = "test_stream", const std::size_t port = 8554);
    void loop();
    void stop();
    void feedData(const char *ptr, std::size_t len);
    bool isStreamStarted() const;
    ~EvaRtspServer();

private:
    std::unique_ptr<TaskScheduler> scheduler{nullptr};
    UsageEnvironment *usage_environment{nullptr};
    RTSPServer *rtspServer{nullptr};
    ServerMediaSession *serverMediaSession;

    DeviceByteSource *realSource{nullptr};
    FramedSource *dataSource{nullptr};

    std::thread mainThread;
    bool mainThreadStarted{false};

    char volatile quit{0};
    bool streamStarted{false};
};

#endif //EVA_EVARTSPSERVER_H
