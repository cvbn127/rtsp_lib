//
// Created by cvbn127 on 17.12.20.
//

#ifndef EVA_H264_CAMERADEVICESOURCE_H
#define EVA_H264_CAMERADEVICESOURCE_H

#include "CameraDeviceSource.h"
#include "DeviceByteSource.h"

const char H264marker[] = {0, 0, 0, 1};
static constexpr int H264markerSize = sizeof(H264marker);

class H264_DeviceByteSource : public DeviceByteSource {
public:
    static H264_DeviceByteSource *
    createNew(UsageEnvironment &env, u_int8_t queueSize, bool repeatConfig);

    virtual ~H264_DeviceByteSource() = default;

    std::string getAuxLine();

protected:
    H264_DeviceByteSource(UsageEnvironment &env, u_int8_t queueSize, bool repeatConfig);

    virtual std::list<std::pair<char *, size_t> >
    splitFrames(char *frame, unsigned frameSize) override;

private:
    bool isStartNal(const unsigned char *ptr) const;

    unsigned char *extractFrame(unsigned char *frame, size_t &size, size_t &outsize);

private:
    std::string m_auxLine{""};
    std::string m_sps{""};
    std::string m_pps{""};
    bool m_repeatConfig;
};


class H264_CameraDeviceSource : public FramedSource {
public:
    static H264_CameraDeviceSource *createNew(UsageEnvironment &env, H264_DeviceByteSource *source) {
        return new H264_CameraDeviceSource(env, source);
    }

    virtual void doGetNextFrame() {
        if (m_inputSource) {
            m_inputSource->getNextFrame(fTo, fMaxSize, afterGettingFrameSub, this,
                                        FramedSource::handleClosure, this);
        }
    }

    virtual void doStopGettingFrames() {
        FramedSource::doStopGettingFrames();
        if (m_inputSource) {
            m_inputSource->stopGettingFrames();
        }
    }

    static void
    afterGettingFrameSub(void *clientData, unsigned frameSize, unsigned numTruncatedBytes,
                         struct timeval presentationTime, unsigned durationInMicroseconds) {
        H264_CameraDeviceSource *source = (H264_CameraDeviceSource *) clientData;
        source->afterGettingFrame(frameSize, numTruncatedBytes, presentationTime,
                                  durationInMicroseconds);
    }

    void afterGettingFrame(unsigned frameSize, unsigned numTruncatedBytes,
                           struct timeval presentationTime, unsigned durationInMicroseconds) {
        fNumTruncatedBytes = numTruncatedBytes;
        fPresentationTime = presentationTime;
        fDurationInMicroseconds = durationInMicroseconds;
        fFrameSize = frameSize;
        afterGetting(this);
    }

    std::string getAuxLine();
protected:
    H264_CameraDeviceSource(UsageEnvironment &env, H264_DeviceByteSource *source) : FramedSource(env),
                                                                     m_inputSource(source) {
    }

    virtual ~H264_CameraDeviceSource() {
        Medium::close(m_inputSource);
    }

protected:
    std::string m_auxLine{""};
    H264_DeviceByteSource *m_inputSource;
};

#endif //EVA_H264_CAMERADEVICESOURCE_H
