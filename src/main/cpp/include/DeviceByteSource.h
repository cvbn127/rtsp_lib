//
// Created by cvbn127 on 08.02.21.
//

#ifndef EVA_DEVICEBYTESOURCE_H
#define EVA_DEVICEBYTESOURCE_H

#include <cstddef>
#include <list>
#include <utility>
#include <string>
#include <pthread.h>


#ifndef _FRAMED_SOURCE_HH

#include "FramedSource.hh"

#endif

class DeviceByteSource : public FramedSource {
public:

    static void
    deliverFrameStub(void *clientData) { ((DeviceByteSource *) clientData)->doGetNextFrame(); };

    static DeviceByteSource *createNew(UsageEnvironment &env, u_int8_t queueSize);

    void feed(const char *ptr, std::size_t len);


    virtual ~DeviceByteSource();

private:

    virtual void doGetNextFrame();

protected:
    DeviceByteSource(UsageEnvironment &env, u_int8_t queueSize);

    static void *
    process_data_stub(
            void *clientData) { return ((DeviceByteSource *) clientData)->process_data(); };

    void *process_data();

    virtual std::list<std::pair<char *, size_t> >
    splitFrames(char *frame, unsigned frameSize);

private:
    u_int8_t queue_size;

    pthread_t process_data_thread_id;
    volatile bool should_stop{false};
    pthread_mutex_t data_mutex;
    pthread_mutex_t frame_mutex;

    std::list<std::pair<char *, std::size_t>> raw_data_list;
    std::list<std::pair<char *, std::size_t> > frame_data_list;

    EventTriggerId deliverFrameEventTriggerId;
};


#endif //EVA_DEVICEBYTESOURCE_H
