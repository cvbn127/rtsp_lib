//
// Created by cvbn127 on 17.12.20.
//

#ifndef EVA_LOG_H
#define EVA_LOG_H

#include <android/log.h>

#define LOG_TAG "Live555Native"

#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define LOGI(...)  __android_log_print(ANDROID_LOG_INFO, LOG_TAG,__VA_ARGS__)
#define LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#endif //EVA_LOG_H
