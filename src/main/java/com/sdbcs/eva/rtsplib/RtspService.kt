package com.sdbcs.eva.rtsplib

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat

enum class CodecType(val value: Int) {
    H264_CODEC_TYPE(0),
    MOTION_JPEG_CODEC_TYPE(1)
}

class RtspService : Service() {

    inner class RtspServiceBinder : Binder() {
        val service: RtspService
            get() = this@RtspService
    }

    private val binder: RtspServiceBinder = RtspServiceBinder()
    private lateinit var nativeRtspServer: NativeRtspServer

    private lateinit var notificationChannel: NotificationChannel

    private var inited: Boolean = false
    private var currentCodecType: CodecType = CodecType.MOTION_JPEG_CODEC_TYPE
    private var currentStreamName: String = "test_stream"
    private var currentStreamUrl: String = ""
    private var currentPort: Int = DEFAULT_RTSP_PORT

    fun init(codecType: CodecType, streamName: String, port: Int = DEFAULT_RTSP_PORT): String {
        if (!inited || (codecType != currentCodecType) ||
            (streamName != currentStreamName) ||
            (port != currentPort)
        ) {
            currentStreamUrl = nativeRtspServer.initialize(codecType.value, streamName, port)
            currentStreamName = streamName
            currentCodecType = codecType
            currentPort = port
            inited = currentStreamUrl.isNotEmpty()
        }
        return currentStreamUrl
    }

    fun stopStream() {
        nativeRtspServer.stop()
        inited = false
    }

    fun isStreamStarted(): Boolean {
        return nativeRtspServer.isStreamStarted()
    }

    fun feed(byteArray: ByteArray) {
        nativeRtspServer.feedData(byteArray)
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private fun startNotification() {
        notificationChannel =
            NotificationChannel(
                CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(notificationChannel)
        val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("A service is running in the background")
            .setContentText("Streaming is ${isStreamStarted()}").build()

        startForeground(NOTIFICATION_ID, notification)
    }

    override fun onCreate() {
        nativeRtspServer = NativeRtspServer()
        startNotification()
        super.onCreate()
    }

    override fun onDestroy() {
        stopStream()
        super.onDestroy()
    }

    companion object {
        const val TAG = "RtspService"
        const val NOTIFICATION_CHANNEL_NAME = "Eva Rtsp Service Notification Channel"
        const val NOTIFICATION_ID = 42

        const val DEFAULT_RTSP_PORT = 8554
        const val CHANNEL_ID = "Eva Rtsp service channel"
    }
}