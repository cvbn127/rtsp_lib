package com.sdbcs.eva.rtsplib

class NativeRtspServer {
    external fun initialize(codecType: Int, streamName: String, port: Int = 8554): String
    external fun isStreamStarted(): Boolean
    external fun stop()
    external fun feedData(byteArray: ByteArray)

    init {
        System.loadLibrary("eva_live555")
    }
}